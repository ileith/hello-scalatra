from tomcat:7.0

maintainer ivor

copy target/*.war /usr/local/tomcat/webapps/hello-scalatra.war

expose 8080

cmd ["catalina.sh", "run"]
